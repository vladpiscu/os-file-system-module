#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <fcntl.h>
#include <dirent.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>

typedef struct {
   char name[15];
   int type;
   int offset;
   int size;
} SECTION;

const int v[7] = {16, 86, 55, 32, 20, 44, 52};

SECTION sect[20];

int typeOK(int type);
int parse(char filename[], int mode);
void findAll(char dirname[]);
void extract(char filename[], int section, int line);
int int_from_string(char number[]);
int size_dir(char dirname[]);
int compute_perm(char perm[], struct stat elem);
void list_files(char dirname[], int recursive, char size_filter[], char perm_filter[], int perm);

int main(int argc, char **argv){
    if(argc >= 2){
        if(strstr(argv[1], "variant")){
            printf("71817\n");
        }
        if(strstr(argv[1], "list"))
        {
            int count = 2;
            int recursive = 0;
            char dirname[1000];
            char size_filter[1000] = "0";
            char perm_filter[1000] = "rwxrwxrwx";
            int perm = 0;
            while(count < argc)
            {
                if(strstr(argv[count], "recursive"))
                {
                    recursive = 1;
                }
                if(strstr(argv[count], "size_greater="))
                {
                    strcpy(size_filter, strchr(argv[count], '=') + 1);
                }
                if(strstr(argv[count], "permissions="))
                {
                    strcpy(perm_filter, strchr(argv[count], '=') + 1);
                    perm = 1;
                }
                if(strstr(argv[count], "path="))
                {
                    strcpy(dirname, strchr(argv[count], '=') + 1);
                }
                count++;
            }
            struct stat fileMetadata;
            if(stat(dirname, &fileMetadata) < 0)
            {
                printf("ERROR\ninvalid directory path");
            }
            else
            {
                if (S_ISDIR(fileMetadata.st_mode))
                {
                    printf("SUCCESS\n");
                    list_files(dirname, recursive, size_filter, perm_filter, perm);
                }

            }
        }
        if(strstr(argv[1], "parse"))
        {
            char filename[1000];
            strcpy(filename, strchr(argv[2], '=') + 1);
            parse(filename, 0);
        }
        if(strstr(argv[1], "extract"))
        {
            char filename[1000];
            char s[100], l[100];
            int count = 2;
            while(count < argc)
            {
                if(strstr(argv[count], "section="))
                {
                    strcpy(s, strchr(argv[count], '=') + 1);
                }
                if(strstr(argv[count], "line="))
                {
                    strcpy(l, strchr(argv[count], '=') + 1);
                }
                if(strstr(argv[count], "path="))
                {
                    strcpy(filename, strchr(argv[count], '=') + 1);
                }
                count++;
            }
            int section; int line;
            section = int_from_string(s);
            line = int_from_string(l);
            extract(filename, section, line);
        }
        if(strstr(argv[1], "findall"))
        {
            char dirname[1000];
            strcpy(dirname, strchr(argv[2], '=') + 1);
            struct stat fileMetadata;
            if(stat(dirname, &fileMetadata) < 0)
            {
                printf("ERROR\ninvalid directory path");
            }
            else
            {
                if (S_ISDIR(fileMetadata.st_mode))
                {
                    printf("SUCCESS\n");
                    findAll(dirname);
                }

            }
        }
    }

    return 0;
}

//This function verifies if the type of the section is in the specified numbers.
int typeOK(int type)
{
    int k = 0;
    for(int i = 0; i < 7; i++)
        if(v[i] == type)
            k = 1;
    return k;
}

//If mode=0, this function does the parsing. If mode=1, the function is looking if there exists at least one section type which is 16.
//It is returning one if there exists one section type with the value 16 or 0 otherwise.
//It is using the typeOk function.
int parse(char filename[], int mode)
{
    int fd;
    int k = 0;
    fd = open(filename, O_RDONLY);
    lseek(fd, -4, SEEK_END);
    int nb_of_sections = 0, version = 0, header_size = 0;
    char magic[3];
    read(fd, &header_size, 2);
    read(fd, &magic, 2);
    lseek(fd, -header_size, SEEK_END);
    read(fd, &version, 4);
    read(fd, &nb_of_sections, 1);
    magic[2] = '\0';
    if(strcmp(magic, "KM") != 0)
    {
        if(mode == 0)
            printf("ERROR\nwrong magic");
        return 0;
    }
    if(version < 25 || version > 83)
    {
        if(mode == 0)
            printf("ERROR\nwrong version");
        return 0;
    }
    if(nb_of_sections < 3 || nb_of_sections > 17)
    {
        if(mode == 0)
            printf("ERROR\nwrong sect_nr");
        return 0;
    }
    for(int i = 0; i < nb_of_sections; i++)
    {
        read(fd, &sect[i].name, 14);
        sect[i].name[14] = '\0';
        read(fd, &sect[i].type, 4);
        if(typeOK(sect[i].type) == 0)
        {
            if(mode == 0)
                printf("ERROR\nwrong sect_types");
            return 0;
        }
        if(mode == 1)
        {
            if(sect[i].type == 16)
                k = 1;
        }
        read(fd, &sect[i].offset, 4);
        read(fd, &sect[i].size, 4);
    }
    if(mode == 0)
    {
        printf("SUCCESS\n");
        printf("version=%d\n", version);
        printf("nr_sections=%d\n", nb_of_sections);
        for(int i = 0; i < nb_of_sections; i++)
        {
            printf("section%d: %s %d %d\n", (i + 1), sect[i].name, sect[i].type, sect[i].size);
        }
    }
    return k;
}

//The function is looking for all the SF files with at least one section type equal to 16. It is using the parse function with mode=1.
void findAll(char dirname[])
{
    DIR *dir;
    dir = opendir(dirname);
    char name[1000];
    struct dirent *dirEntry;
    struct stat elem;
    while((dirEntry = readdir(dir)) != 0)
    {
        strcpy(name, dirname);
        strcat(name, "/");
        strcat(name, dirEntry->d_name);
        lstat(name, &elem);
        if(strcmp(dirEntry->d_name, ".") != 0 && strcmp(dirEntry->d_name, "..") != 0)
        {
            if(S_ISDIR(elem.st_mode))
                findAll(name);
            else
            {
                if(parse(name, 1) == 1)
                    printf("%s\n", name);
            }
        }
    }
    free(dir);
    free(dirEntry);
}


//The function is extracting a line from a SF file, in reversed order.
//It is using the strrchr function, which is looking for the last character equal with the one from its parameters.
//Until we find the line we are looking for, the program is minimizing the buffer with the character '\0', in order to have different results with strrchr.
void extract(char filename[], int section, int line)
{
    int fd;
    fd = open(filename, O_RDONLY);
    lseek(fd, -4, SEEK_END);
    int nb_of_sections = 0, version = 0, header_size = 0;
    char magic[3];
    read(fd, &header_size, 2);
    read(fd, &magic, 2);
    lseek(fd, -header_size, SEEK_END);
    read(fd, &version, 4);
    read(fd, &nb_of_sections, 1);
    magic[2] = '\0';
    if(strcmp(magic, "KM") != 0)
    {
        printf("ERROR\ninvalid file");
        return;
    }
    if(version < 25 || version > 83)
    {
        printf("ERROR\nwinvalid file");
        return;
    }
    if(nb_of_sections < 3 || nb_of_sections > 17)
    {
        printf("ERROR\ninvalid file");
        return;
    }
    for(int i = 0; i < nb_of_sections; i++)
    {
        read(fd, &sect[i].name, 14);
        sect[i].name[14] = '\0';
        read(fd, &sect[i].type, 4);
        if(typeOK(sect[i].type) == 0)
        {
            printf("ERROR\ninvalid file");
            return;
        }
        read(fd, &sect[i].offset, 4);
        read(fd, &sect[i].size, 4);
    }
    section--;
    if(section > nb_of_sections)
    {
        printf("ERROR\ninvalid section");
        return;
    }
    lseek(fd, sect[section].offset, SEEK_SET);
    char *buffer = malloc((sect[section].size));
    read(fd, buffer, sect[section].size);
    int counter = 1;
    char *pch;
    pch = strrchr(buffer, '\x0A');
    if(line > 1)
    {
        counter = 2;
        while(pch != NULL && counter < line)
        {
            *pch = '\0';
            pch = strrchr(buffer, '\x0A');
            counter++;
        }
    }
    if(pch == NULL)
    {
        printf("ERROR\ninvalid line");
        return;
    }
    pch--;
    printf("SUCCESS\n");
    while(*pch != '\x0A' && pch >= buffer)
    {
        printf("%c", *pch);
        pch--;
    }
    free(buffer);
    printf("\n");
}

int int_from_string(char number[])
{
    int i = 0;
    int num = 0;
    while(i < strlen(number))
    {
        num = num * 10 + number[i] - '0';
        i++;
    }
    return num;
}


//This function is calculating the binary equivalent of the permissions string.
//It is returning 1 if the permissions of the file is the same as the permissions we are looking for and 0 otherwise.
int compute_perm(char perm[], struct stat elem)
{
    int a = 0;
    int r = 0x04, w = 0x02, x = 0x01;
    for(int i = 0; i < 9; i++)
    {
        if(perm[i] == 'r')
            a += r << ((2 - (i / 3)) * 3);
        if(perm[i] == 'w')
            a += w << ((2 - (i / 3)) * 3);
        if(perm[i] == 'x')
            a += x << ((2 - (i / 3)) * 3);
    }
    if((elem.st_mode & 0x01FF) == a)
        return 1;
    return 0;
}


//This function can be used for all the variants of listing the files. If the recursive parameter is 1, it will do the recursive version of listing.
//If the value represented by size_filter is greater than 0, it will be looking also for the size of the files.
//If perm parameter is 1, the function will be looking for the permission rights.
void list_files(char dirname[], int recursive, char size_filter[], char perm_filter[], int perm)
{
    int size = int_from_string(size_filter);
    DIR *dir;
    dir = opendir(dirname);
    char name[1000];
    struct dirent *dirEntry;
    struct stat elem;
    while((dirEntry = readdir(dir)) != 0)
    {
        strcpy(name, dirname);
        strcat(name, "/");
        strcat(name, dirEntry->d_name);
        lstat(name, &elem);
        if(strcmp(dirEntry->d_name, ".") != 0 && strcmp(dirEntry->d_name, "..") != 0)
        {
            if(S_ISDIR(elem.st_mode))
            {
                if(perm == 1)
                {
                    if(compute_perm(perm_filter, elem) == 1)
                    {
                            printf("%s\n", name);
                    }
                }
                else
                {
                    if(size == 0)
                        printf("%s\n", name);
                }
                if(recursive == 1)
                    list_files(name, recursive, size_filter, perm_filter, perm);
            }
            else
            {
                if(perm == 1)
                {
                    if(compute_perm(perm_filter, elem) == 1)
                    {
                        if(size > 0)
                        {
                            if(elem.st_size >= size && S_ISREG(elem.st_mode))
                                printf("%s\n", name);
                        }
                        else
                            printf("%s\n", name);
                    }
                }
                else
                {
                    if(size > 0)
                    {
                        if(elem.st_size >= size && S_ISREG(elem.st_mode))
                            printf("%s\n", name);
                    }
                    else
                        printf("%s\n", name);
                }
            }
        }
    }
    free(dir);
    free(dirEntry);
}

//This function is computing the total size of a directory. It is no longer used in the final version of the program.
//The first version of the tester.py was looking for directories, while the newer version is not.
/*
int size_dir(char dirname[])
{
    int size = 0;
    DIR *dir;
    dir = opendir(dirname);
    char name[1000];
    struct dirent *dirEntry;
    struct stat elem;
    while((dirEntry = readdir(dir)) != 0)
    {
        strcpy(name, dirname);
        strcat(name, "/");
        strcat(name, dirEntry->d_name);
        lstat(name, &elem);
        if(S_ISDIR(elem.st_mode) && strcmp(dirEntry->d_name, ".") != 0 && strcmp(dirEntry->d_name, "..") != 0)
        {
            size = size + size_dir(name);
        }
        else
            size += elem.st_size;
    }
    return size;
}
*/
